(defun by-5 (x)
  (* x 5))

;; tests
(lisp-unit:define-test by-5
  (lisp-unit:assert-equal 10 (by-5 2))
  (lisp-unit:assert-equal 20 (by-5 4))
  (lisp-unit:assert-equal 25 (by-5 4)))
