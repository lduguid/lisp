(defun hello-world ()
  (format t "hello, world!"))

;; An example of the simple form of LOOP
(defun sqrt-advisor()
  (loop (format t "~&Number: ")
     (let ((n (parse-integer (read-line) :junk-allowed t)))
       (when (not n) (return))
       (format t "~&the square root of ~D is ~D.~%" n (sqrt n)))))

;; An example of the extended form of a loop
(defun square-advisor()
  (loop as n = (progn (format t "~&Number: ")
		      (parse-integer (read-line) :junk-allowed t))
       while n
       do (format t "~&The square of ~D is ~D.~%" n (* n n))))

;; Another example of the extended form of LOOP.
(defun my-odd()
  (loop for n from 1 to 10
     when (oddp n)
     collect n))
